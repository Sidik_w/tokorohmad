<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/tokorohmad/admin/');
define('HTTP_CATALOG', 'http://localhost/tokorohmad/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/tokorohmad/admin/');
define('HTTPS_CATALOG', 'http://localhost/tokorohmad/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/tokorohmad/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/tokorohmad/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/tokorohmad/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/tokorohmad/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/tokorohmad/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/tokorohmad/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/tokorohmad/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/tokorohmad/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/tokorohmad/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/tokorohmad/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/tokorohmad/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/tokorohmad/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'dbrohmad');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
